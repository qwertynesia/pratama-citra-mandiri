<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends CI_Controller {

	public function index()
	{
		session_start();
		if(!empty($_SESSION['levels'])){
			$this->load->helper('url');
			$this->load->helper('form');
			
			$this->load->view('main/header');
			$this->load->view('main/view_home');
			$this->load->view('main/footer');
		}else{
			redirect('login/index?err=3','refresh');
		}
	}
	
	public function user()
	{
		session_start();
		if(!empty($_SESSION['levels'])){
			$this->load->helper('url');
			$this->load->helper('form');
			
			$this->load->view('main/header');
			$this->load->model('Model_home');
			$data=$this->Model_home->user();
			$this->load->view('master/view_user',$data);
			$this->load->view('main/footer');
		}else{
			redirect('login/index?err=3','refresh');
		}
	}
	
	public function usertambah()
	{
		session_start();
		if(!empty($_SESSION['levels'])){
			$this->load->helper('url');
			$this->load->helper('form');
			
			$this->load->view('main/header');
			$this->load->model('Model_home');
			$this->load->view('master/view_usertambah');
			$this->load->view('main/footer');
		}else{
			redirect('login/index?err=3','refresh');
		}
	}
	
	public function usertambahact()
	{
		session_start();
		if(!empty($_SESSION['levels'])){
			$this->load->helper('url');
			$this->load->helper('form');
			
			$this->load->model('Model_home');
			$data['query']=$this->Model_home->usertambah();
			redirect('home/user','refresh');
		}else{
			redirect('login/index?err=3','refresh');
		}
	}
	
	public function useredit($p)
	{
		session_start();
		if(!empty($_SESSION['levels'])){
			$this->load->helper('url');
			$this->load->helper('form');
			
			$this->load->view('main/header');
			$this->load->model('Model_home');
			$data['p']=$p;
			$data['query']=$this->Model_home->useredit($p);
			$this->load->view('master/view_useredit',$data);
			$this->load->view('main/footer');
		}else{
			redirect('login/index?err=3','refresh');
		}
	}
	
	public function usereditact($p)
	{
		session_start();
		if(!empty($_SESSION['levels'])){
			$this->load->helper('url');
			$this->load->helper('form');
			
			$this->load->model('Model_home');
			$data['query']=$this->Model_home->usereditact($p);
			redirect('home/user','refresh');
		}else{
			redirect('login/index?err=3','refresh');
		}
	}
	
	public function userhapus($p)
	{
		session_start();
		if(!empty($_SESSION['levels'])){
			$this->load->helper('url');
			$this->load->helper('form');
			
			$this->load->model('Model_home');
			$data['query']=$this->Model_home->userhapus($p);
			redirect('home/user','refresh');
		}else{
			redirect('login/index?err=3','refresh');
		}
	}
	
	public function customer()
	{
		session_start();
		if(!empty($_SESSION['levels'])){
			$this->load->helper('url');
			$this->load->helper('form');
			
			$this->load->view('main/header');
			$this->load->model('Model_home');
			$data=$this->Model_home->customer();
			$this->load->view('master/view_customer',$data);
			$this->load->view('main/footer');
		}else{
			redirect('login/index?err=3','refresh');
		}
	}
	
	public function customertambah()
	{
		session_start();
		if(!empty($_SESSION['levels'])){
			$this->load->helper('url');
			$this->load->helper('form');
			
			$this->load->view('main/header');
			$this->load->model('Model_home');
			$this->load->view('master/view_customertambah');
			$this->load->view('main/footer');
		}else{
			redirect('login/index?err=3','refresh');
		}
	}
	
	public function customertambahact($p)
	{
		session_start();
		//if(!empty($_SESSION['levels'])){
			$this->load->helper('url');
			$this->load->helper('form');
			
			$this->load->model('Model_home');
			$data['query']=$this->Model_home->customertambah();
			if($p==1){
				redirect('home/customer','refresh');
			}else{
				redirect('login/login?err=4','refresh');
			}
		//}else{
			redirect('login/index?err=3','refresh');
		//}
	}
	
	public function customeredit($p)
	{
		session_start();
		if(!empty($_SESSION['levels'])){
			$this->load->helper('url');
			$this->load->helper('form');
			
			$this->load->view('main/header');
			$this->load->model('Model_home');
			$data['p']=$p;
			$data['query']=$this->Model_home->customeredit($p);
			$this->load->view('master/view_customeredit',$data);
			$this->load->view('main/footer');
		}else{
			redirect('login/index?err=3','refresh');
		}
	}
	
	public function customereditact($p)
	{
		session_start();
		if(!empty($_SESSION['levels'])){
			$this->load->helper('url');
			$this->load->helper('form');
			
			$this->load->model('Model_home');
			$data['query']=$this->Model_home->customereditact($p);
			redirect('home/customer','refresh');
		}else{
			redirect('login/index?err=3','refresh');
		}
	}
	
	public function customerhapus($p)
	{
		session_start();
		if(!empty($_SESSION['levels'])){
			$this->load->helper('url');
			$this->load->helper('form');
			
			$this->load->model('Model_home');
			$data['query']=$this->Model_home->customerhapus($p);
			redirect('home/customer','refresh');
		}else{
			redirect('login/index?err=3','refresh');
		}
	}
	
	public function kategoriproduk()
	{
		session_start();
		if(!empty($_SESSION['levels'])){
			$this->load->helper('url');
			$this->load->helper('form');
			
			$this->load->view('main/header');
			$this->load->model('Model_home');
			$data=$this->Model_home->kategoriproduk();
			$this->load->view('master/view_kategoriproduk',$data);
			$this->load->view('main/footer');
		}else{
			redirect('login/index?err=3','refresh');
		}
	}
	
	public function kategoriproduktambah()
	{
		session_start();
		if(!empty($_SESSION['levels'])){
			$this->load->helper('url');
			$this->load->helper('form');
			
			$this->load->view('main/header');
			$this->load->model('Model_home');
			$this->load->view('master/view_kategoriproduktambah');
			$this->load->view('main/footer');
		}else{
			redirect('login/index?err=3','refresh');
		}
	}
	
	public function kategoriproduktambahact()
	{
		session_start();
		if(!empty($_SESSION['levels'])){
			$this->load->helper('url');
			$this->load->helper('form');
			
			$this->load->model('Model_home');
			$data['query']=$this->Model_home->kategoriproduktambah();
			redirect('home/kategoriproduk','refresh');
		}else{
			redirect('login/index?err=3','refresh');
		}
	}
	
	public function kategoriprodukedit($p)
	{
		session_start();
		if(!empty($_SESSION['levels'])){
			$this->load->helper('url');
			$this->load->helper('form');
			
			$this->load->view('main/header');
			$this->load->model('Model_home');
			$data['p']=$p;
			$data['query']=$this->Model_home->kategoriprodukedit($p);
			$this->load->view('master/view_kategoriprodukedit',$data);
			$this->load->view('main/footer');
		}else{
			redirect('login/index?err=3','refresh');
		}
	}
	
	public function kategoriprodukeditact($p)
	{
		session_start();
		if(!empty($_SESSION['levels'])){
			$this->load->helper('url');
			$this->load->helper('form');
			
			$this->load->model('Model_home');
			$data['query']=$this->Model_home->kategoriprodukeditact($p);
			redirect('home/kategoriproduk','refresh');
		}else{
			redirect('login/index?err=3','refresh');
		}
	}
	
	public function kategoriprodukhapus($p)
	{
		session_start();
		if(!empty($_SESSION['levels'])){
			$this->load->helper('url');
			$this->load->helper('form');
			
			$this->load->model('Model_home');
			$data['query']=$this->Model_home->kategoriprodukhapus($p);
			redirect('home/kategoriproduk','refresh');
		}else{
			redirect('login/index?err=3','refresh');
		}
	}
	
	public function produk()
	{
		session_start();
		if(!empty($_SESSION['levels'])){
			$this->load->helper('url');
			$this->load->helper('form');
			
			$this->load->view('main/header');
			$this->load->model('Model_home');
			$data=$this->Model_home->produk();
			$this->load->view('master/view_produk',$data);
			$this->load->view('main/footer');
		}else{
			redirect('login/index?err=3','refresh');
		}
	}
	
	public function produktambah()
	{
		session_start();
		if(!empty($_SESSION['levels'])){
			$this->load->helper('url');
			$this->load->helper('form');
			
			$this->load->view('main/header');
			$this->load->model('Model_home');
			$data['kategoriproduk']=$this->Model_home->pilihkategoriproduk();
			$this->load->view('master/view_produktambah',$data);
			$this->load->view('main/footer');
		}else{
			redirect('login/index?err=3','refresh');
		}
	}
	
	public function produktambahact()
	{
		session_start();
		if(!empty($_SESSION['levels'])){
			$this->load->helper('url');
			$this->load->helper('form');
			
			$this->load->model('Model_home');
			$data['query']=$this->Model_home->produktambah();
			redirect('home/produk','refresh');
		}else{
			redirect('login/index?err=3','refresh');
		}
	}
	
	public function produkedit($p)
	{
		session_start();
		if(!empty($_SESSION['levels'])){
			$this->load->helper('url');
			$this->load->helper('form');
			
			$this->load->view('main/header');
			$this->load->model('Model_home');
			$data['p']=$p;
			$data['query']=$this->Model_home->produkedit($p);
			$data['kategoriproduk']=$this->Model_home->pilihkategoriproduk();
			$this->load->view('master/view_produkedit',$data);
			$this->load->view('main/footer');
		}else{
			redirect('login/index?err=3','refresh');
		}
	}
	
	public function produkeditact($p)
	{
		session_start();
		if(!empty($_SESSION['levels'])){
			$this->load->helper('url');
			$this->load->helper('form');
			
			$this->load->model('Model_home');
			$data['query']=$this->Model_home->produkeditact($p);
			redirect('home/produk','refresh');
		}else{
			redirect('login/index?err=3','refresh');
		}
	}
	
	public function produkhapus($p)
	{
		session_start();
		if(!empty($_SESSION['levels'])){
			$this->load->helper('url');
			$this->load->helper('form');
			
			$this->load->model('Model_home');
			$data['query']=$this->Model_home->produkhapus($p);
			redirect('home/produk','refresh');
		}else{
			redirect('login/index?err=3','refresh');
		}
	}
	
}