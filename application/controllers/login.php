<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends CI_Controller {

	public function index()
	{
		session_start();
		if(empty($_SESSION['levels'])){
			$this->load->helper('url');
			$this->load->helper('form');
			$this->load->view('main/view_login');
		}else{
			redirect('home/index','refresh');
		}
	}
	
	public function ceklogin()
    {
		$this->load->helper('url');
		$this->load->helper('form');
	
		$username = $this->input->post('username'); //sama dengan $_POST['username'];
		$password = $this->input->post('password');
		$password = md5("$password"); //utk enskripsi password
		
		$this->load->model('Model_login');
		if($this->Model_login->check_user($username,$password) == TRUE){
			redirect('home','refresh');
		}else{
			redirect('login/login?err=1','refresh');
		}
	}
	
	public function logout()
    {
		session_start();
		session_destroy();
		if($_SESSION['levels']!="Customer"){
			redirect('login/login?err=2','refresh');
		}else{
			redirect('customer/index?err=2','refresh');
		}
	}
	
	public function profil()
	{
		session_start();
		if(!empty($_SESSION['levels'])){
			$this->load->helper('url');
			$this->load->helper('form');
			
			$this->load->view('main/header');
			$this->load->model('Model_login');
			$data['query']=$this->Model_login->profil();
			$this->load->view('master/view_profil',$data);
			$this->load->view('main/footer');
		}else{
			redirect('login/index?err=3','refresh');
		}
	}
	
	public function daftar()
	{
		session_start();
		$this->load->helper('url');
		$this->load->helper('form');
		
		$this->load->view('main/header');
		$this->load->view('customer/view_daftar');
		$this->load->view('main/footer');
	}
	
}