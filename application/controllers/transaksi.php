<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Transaksi extends CI_Controller {

	public function index()
	{
		session_start();
		if(!empty($_SESSION['levels'])){
			$this->load->helper('url');
			$this->load->helper('form');
			
			$this->load->view('main/header');
			$this->load->view('main/view_home');
			$this->load->view('main/footer');
		}else{
			redirect('login/index?err=3','refresh');
		}
	}
	
	public function dataorder()
	{
		session_start();
		if(!empty($_SESSION['levels'])){
			$this->load->helper('url');
			$this->load->helper('form');
			
			$this->load->view('main/header');
			$this->load->model('Model_transaksi');
			$data=$this->Model_transaksi->dataorder();
			$this->load->view('transaksi/view_order',$data);
			$this->load->view('main/footer');
		}else{
			redirect('login/index?err=3','refresh');
		}
	}
	
	public function orderdetail($p)
	{
		session_start();
		if(!empty($_SESSION['levels'])){
			$this->load->helper('url');
			$this->load->helper('form');
			
			$this->load->view('main/header');
			$this->load->model('Model_transaksi');
			$data['query']=$this->Model_transaksi->orderdetail($p);
			$data['querys']=$this->Model_transaksi->orderpemesan($p);
			$data['p']=$p;
			$this->load->view('transaksi/view_orderdetail',$data);
			$this->load->view('main/footer');
		}else{
			redirect('login/index?err=3','refresh');
		}
	}
	
	public function konfirmasibayar()
	{
		session_start();
		if(!empty($_SESSION['levels'])){
			$this->load->helper('url');
			$this->load->helper('form');
			
			$this->load->view('main/header');
			$this->load->model('Model_transaksi');
			$data=$this->Model_transaksi->konfirmasibayar();
			$this->load->view('transaksi/view_konfirmasibayar',$data);
			$this->load->view('main/footer');
		}else{
			redirect('login/index?err=3','refresh');
		}
	}
	
	public function keranjang($p,$q)
	{
		session_start();
		if(!empty($_SESSION['levels'])){
			$this->load->helper('url');
			$this->load->helper('form');
			
			$this->load->view('main/header');
			$this->load->model('Model_transaksi');
			$data['status']=$this->Model_transaksi->keranjang($p,$q);
			$data['query']=$this->Model_transaksi->keranjangproduk();
			$this->load->view('transaksi/view_keranjang',$data);
			$this->load->view('main/footer');
		}else{
			redirect('login/index?err=5','refresh');
		}
	}
	
	public function pesan($p)
	{
		session_start();
		if(!empty($_SESSION['levels'])){
			$this->load->helper('url');
			$this->load->helper('form');
			
			$this->load->view('main/header');
			$this->load->model('Model_transaksi');
			$submit=$_POST['submit'];
			if($submit=="Checkout"){
				$data['query']=$this->Model_transaksi->checkout($p);
				$data['querys']=$this->Model_transaksi->checkoutpemesan($p);
			}elseif($submit=="Pesan"){
				$data['query']=$this->Model_transaksi->pesan($p);
			}
			$data['p']=$p;
			$data['submit']=$submit;
			$this->load->view('transaksi/view_checkout',$data);
			$this->load->view('main/footer');
		}else{
			redirect('login/index?err=5','refresh');
		}
	}
	
	public function mengirimbarang($p)
	{
		session_start();
		if(!empty($_SESSION['levels'])){
			$this->load->helper('url');
			$this->load->helper('form');
			
			$this->load->model('Model_transaksi');
			$data['query']=$this->Model_transaksi->mengirimbarang($p);
			redirect('transaksi/orderdetail/'.$p,'refresh');
		}else{
			redirect('login/index?err=5','refresh');
		}
	}
	
	public function konfirmasi($p)
	{
		session_start();
		if(!empty($_SESSION['levels'])){
			$this->load->helper('url');
			$this->load->helper('form');
			
			$this->load->model('Model_transaksi');
			$data['query']=$this->Model_transaksi->konfirmasi($p);
			redirect('transaksi/konfirmasibayar','refresh');
		}else{
			redirect('login/index?err=5','refresh');
		}
	}
	
	public function konfirmasibayartambah()
	{
		session_start();
		if(!empty($_SESSION['levels'])){
			$this->load->helper('url');
			$this->load->helper('form');
			
			$this->load->view('main/header');
			$this->load->model('Model_transaksi');
			$data['query']=$this->Model_transaksi->dataorderpilihan();
			if(!empty($_POST['idorder'])){
				$data['querys']=$this->Model_transaksi->dataorderpilihan2();
			}
			$this->load->view('transaksi/view_konfirmasibayartambah',$data);
			$this->load->view('main/footer');
		}else{
			redirect('login/index?err=3','refresh');
		}
	}
	
	public function konfirmasibayartambahact()
	{
		session_start();
		if(!empty($_SESSION['levels'])){
			$this->load->helper('url');
			$this->load->helper('form');
			
			$this->load->model('Model_transaksi');
			$data['query']=$this->Model_transaksi->konfirmasibayartambah();
			redirect('transaksi/konfirmasibayar','refresh');
		}else{
			redirect('login/index?err=5','refresh');
		}
	}
	
}