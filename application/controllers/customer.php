<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Customer extends CI_Controller {

	public function index()
	{
		session_start();
		$this->load->helper('url');
		$this->load->helper('form');
		
		$this->load->view('main/header');
		$this->load->view('customer/view_home');
		$this->load->view('main/footer');
	}
	
	public function produk($p)
	{
		session_start();
		$this->load->helper('url');
		$this->load->helper('form');
		
		$this->load->view('main/header');
		$this->load->model('Model_customer');
		$data=$this->Model_customer->produk($p);
		$this->load->view('customer/view_produk',$data);
		$this->load->view('main/footer');
	}
	
	public function tentangkami()
	{
		session_start();
		$this->load->helper('url');
		$this->load->helper('form');
		
		$this->load->view('main/header');
		$this->load->view('customer/view_tentangkami');
		$this->load->view('main/footer');
	}
	
	public function hubungikami()
	{
		session_start();
		$this->load->helper('url');
		$this->load->helper('form');
		
		$this->load->view('main/header');
		$this->load->view('customer/view_hubungikami');
		$this->load->view('main/footer');
	}
	
	public function faq()
	{
		session_start();
		$this->load->helper('url');
		$this->load->helper('form');
		
		$this->load->view('main/header');
		$this->load->view('customer/view_faq');
		$this->load->view('main/footer');
	}
	
}