<div class="main">
  <div class="main-inner">
	<div class="container">
	   <div class="row all-icons">    
		<div class="widget">
			<div class="widget-header">
				<i class="icon-list-alt"></i>
				<h3>Data Konfirmasi Pembayaran</h3>
			</div>
			<!-- /widget-header -->
			<div class="widget-content">
			<p>
				<?php echo form_open('transaksi/konfirmasibayar'); ?>
					<table>
						<tr>
							<td>Cari</td>
							<td>:</td>
							<td><input type="text" name="cari" value="" onchange="submit()" required></td>
						</tr>
					</table>
				<?php echo form_close(); ?>
				<?php echo anchor('transaksi/konfirmasibayartambah', 'Tambah', 'Style="color:#fff;"');?>
				<table width="100%" class="table table-striped table-bordered">
					<tr>
						<th>No</th>
						<th>Tanggal Konfirmasi</th>
						<th>ID Order</th>
						<th>Nama Pengirim</th>
						<th>No Rekening</th>
						<th>Bank</th>
						<th>Jumlah Transfer</th>
						<th>Bukti Transfer</th>
						<th>Status</th>
						<?php
							if($_SESSION['levels']!="Customer"){
						?>
						<th colspan="2">Aksi</th>
						<?php
							}
						?>
					</tr>
					<?php
						$no=1;
						if(empty($offset)){
							$no=1;
						}else{
							$no=$no+$offset;
						}
						$jml=0;
						foreach($query->result() as $row){
					?>
					<tr>
						<td><?php echo"$no";?></td>
						<td><?php echo $row->tgl;?></td>
						<td><?php echo $row->idorder;?></td>
						<td><?php echo $row->namapengirim;?></td>
						<td><?php echo $row->norek;?></td>
						<td><?php echo $row->bank;?></td>
						<td><?php echo $row->jmltransfer;?></td>
						<td><?php echo $row->buktitransfer;?></td>
						<td><?php echo $row->status;?></td>
						<?php
							if($_SESSION['levels']!="Customer"){
								if($row->status=="Belum Acc"){
						?>
						<td><?php echo anchor('transaksi/konfirmasi/'.$row->idkonfirmasipembayaran, 'Konfirmasi');?></td>
						<?php
							}else{
						?>
						<td>-</td>
						<?php
								}
							}
						?>
					</tr>
					<?php
							$jml++;
							$no++;
						}if($jml==0){
					?>
					<tr>
						<td style="color:#ff0000; font-weight:bold; text-align:center;" colspan="11"><b>Belum terdapat data.</b></td>
					</tr>
					<?php
						}
					?>
				</table>
				<p align="center"><?php echo $this->pagination->create_links();?></p>
			</p>
			</div>
		</div>
	  </div> <!-- /row -->
	</div> <!-- /container -->
  </div> <!-- /main-inner -->
</div>