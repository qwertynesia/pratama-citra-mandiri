<div class="main">
  <div class="main-inner">
	<div class="container">
	   <div class="row all-icons">    
		<div class="widget">
			<div class="widget-header">
				<i class="icon-list-alt"></i>
				<h3>Data Order</h3>
			</div>
			<!-- /widget-header -->
			<div class="widget-content">
			<p>
				<?php echo form_open('transaksi/orderukuran'); ?>
					<table>
						<tr>
							<td>Cari</td>
							<td>:</td>
							<td><input type="text" name="cari" value="" onchange="submit()" required></td>
						</tr>
					</table>
				<?php echo form_close(); ?>
				<table width="100%" class="table table-striped table-bordered">
					<tr>
						<th>No</th>
						<th>Tanggal Order</th>
						<th>ID Order</th>
						<th>Nama Customer</th>
						<th>Biaya Kirim</th>
						<th>Sub Total Pembelian</th>
						<th>Total Pembelian</th>
						<th>Status Alamat</th>
						<th>Status Order</th>
						<th colspan="3">Aksi</th>
					</tr>
					<?php
						$no=1;
						if(empty($offset)){
							$no=1;
						}else{
							$no=$no+$offset;
						}
						$jml=0;
						foreach($query->result() as $row){
					?>
					<tr>
						<td><?php echo"$no";?></td>
						<td><?php echo $row->tgl;?></td>
						<td><?php echo $row->idorder;?></td>
						<td><?php echo $row->nama;?></td>
						<td><?php echo $row->biayakirim;?></td>
						<td><?php echo $row->subtotal;?></td>
						<td><?php echo $row->total;?></td>
						<td><?php echo $row->statusalamat;?></td>
						<td><?php echo $row->status;?></td>
						<td><?php echo anchor('transaksi/orderdetail/'.$row->idorder, 'Detail');?></td>
					</tr>
					<?php
							$jml++;
							$no++;
						}if($jml==0){
					?>
					<tr>
						<td style="color:#ff0000; font-weight:bold; text-align:center;" colspan="11"><b>Belum terdapat data.</b></td>
					</tr>
					<?php
						}
					?>
				</table>
				<p align="center"><?php echo $this->pagination->create_links();?></p>
			</p>
			</div>
		</div>
	  </div> <!-- /row -->
	</div> <!-- /container -->
  </div> <!-- /main-inner -->
</div>