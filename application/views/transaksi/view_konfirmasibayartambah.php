<div class="main">
  <div class="main-inner">
	<div class="container">
	   <div class="row all-icons">    
		<div class="widget">
			<div class="widget-header">
				<i class="icon-list-alt"></i>
				<h3>Tambah Konfirmasi Pembayaran</h3>
			</div>
			<!-- /widget-header -->
			<div class="widget-content">
			<p>
				<?php
					$idorder="";
					if(!empty($_POST['idorder'])){
						$idorder=$_POST['idorder'];
					}
				?>
				<?php echo form_open('transaksi/konfirmasibayartambah'); ?>
				<table>
					<tr>
						<td>ID Order</td>
						<td>:</td>
						<td>
						<select name="idorder" required>
							<option value="<?php echo $idorder;?>"><?php echo $idorder;?></option>
							<?php
								foreach($query as $row){
							?>
							<option value="<?php echo $row->idorder;?>"><?php echo $row->idorder;?></option>
							<?php
								}
							?>
						</select>
						<input type="submit" name="submit" value="Pilih"></td>
					</tr>
				<?php echo form_close(); ?>
				<?php
					if($idorder!=""){
						foreach($querys as $rows){
							$total=$rows->total;
						}
				?>
				<?php echo form_open('transaksi/konfirmasibayartambahact'); ?>
					<input type="hidden" name="idorder" value="<?php echo $idorder;?>" required>
					<tr>
						<td>Nama Pengirim</td>
						<td>:</td>
						<td><input type="text" name="namapengirim" value="" required></td>
					</tr>
					<tr>
						<td>Nomor Rekening</td>
						<td>:</td>
						<td><input type="number" name="norek" value="" required></td>
					</tr>
					<tr>
						<td>Bank</td>
						<td>:</td>
						<td>
						<select name="bank" required>
							<option value="Mandiri">Mandiri</option>
							<option value="BNI">BNI</option>
						</select>
						</td>
					</tr>	
					<tr>
						<td>Jumlah Transfer</td>
						<td>:</td>
						<td><input type="number" name="jmltransfer" value="<?php echo $total;?>" readonly required></td>
					</tr>
					<tr>
						<td>Bukti Transfer</td>
						<td>:</td>
						<td><input type="text" name="buktitransfer" value="" required></td>
					</tr>
					<tr>
						<td colspan="3"><input type="submit" name="submit" value="Simpan"></td>
					</tr>
				<?php echo form_close(); ?>
				<?php
					}
				?>
				</table>
			</p>
			</div>
		</div>
	  </div> <!-- /row -->
	</div> <!-- /container -->
  </div> <!-- /main-inner -->
</div>