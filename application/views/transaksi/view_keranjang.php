<div class="main">
  <div class="main-inner">
	<div class="container">
	   <div class="row all-icons">    
		<div class="widget">
			<div class="widget-header">
				<i class="icon-list-alt"></i>
				<h3>Keranjang Belanja</h3>
			</div>
			<!-- /widget-header -->
			<div class="widget-content">
			<p>
				<?php
					if($status==1){
						echo"<font color='#6A9B25'>Data Berhasil dimasukkan kedalam keranjang belanja.</font>";
					}else if($status==2){
						echo"<font color='#ff0000'>Data sudah berada dikeranjang belanja. Silahkan update jumlah untuk menambah jumlah barang.</font>";
					}else if($status==3){
						echo"<font color='#6A9B25'>Jumlah Produk berhasil diedit.</font>";
					}else if($status==4){
						echo"<font color='#6A9B25'>Data Berhasil dihapus.</font>";
					}
				?>
				<table width="100%" class="table table-striped table-bordered">
					<?php
						$no=1;
						$totalharga=0;
						foreach($query as $row){
							$idorder=$row->idorder;
					?>
					<?php echo form_open('transaksi/keranjang/'.$row->idproduk.'/2'); ?>
					<input type="hidden" style="width:50px;" name="idorder" value="<?php echo $row->idorder;?>">
					<tr>
						<th width="20%">No</th>
						<th width="1%">:</th>
						<td><?php echo"$no";?></td>
					</tr>
					<tr>
						<th>Kode Produk</th>
						<th>:</th>
						<td><?php echo $row->idproduk;?></td>
					</tr>
					<tr>
						<th>Nama Produk</th>
						<th>:</th>
						<td><?php echo $row->namaproduk;?></td>
					</tr>
					<tr>
						<th>Harga</th>
						<th>:</th>
						<td><?php echo $row->harga;?></td>
					</tr>
					<tr>
						<th>Jumlah</th>
						<th>:</th>
						<td><input type="number" style="width:50px;" name="jml" value="<?php echo $row->jml;?>"></td>
					</tr>
					<?php
						$subharga=$row->harga*$row->jml;
						$totalharga=$totalharga+$subharga;
					?>
					<tr>
						<th>Sub Harga</th>
						<th>:</th>
						<th align="right"><?php echo $subharga;?></th>
					</tr>
					<tr>
						<td colspan="3">
							<input type="submit" name="submit" value="Ubah">
							<input type="submit" name="submit" value="Hapus">
						</td>
					</tr>
					<tr>
						<td colspan="3">&nbsp;</td>
					</tr>
					<?php echo form_close(); ?>
					<?php
							$no++;
						}
					?>
					<tr>
						<th>Total</th>
						<th>:</th>
						<th align="right"><?php echo $totalharga;?></th>
					</tr>
				</table>
				<?php echo form_open('transaksi/pesan/'.$idorder); ?>
					<div style="text-align:right;"><input type="submit" name="submit" value="Checkout"></div>
				<?php echo form_close(); ?>
			</p>
			</div>
		</div>
	  </div> <!-- /row -->
	</div> <!-- /container -->
  </div> <!-- /main-inner -->
</div>