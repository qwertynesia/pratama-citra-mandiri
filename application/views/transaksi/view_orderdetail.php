<div class="main">
  <div class="main-inner">
	<div class="container">
	   <div class="row all-icons">    
		<div class="widget">
			<div class="widget-header">
				<i class="icon-list-alt"></i>
				<h3>Detail Order <?php echo $p;?></h3>
			</div>
			<!-- /widget-header -->
			<div class="widget-content">
			<p>
			<h3>Data Pemesanan</h3>
			<?php
				foreach($querys as $row){
					$this->load->database();
					$idorder=$row->idorder;
					$statusorder=$row->status;
					$querycek = $this->db->query("
						select * from 
							konfirmasipembayaran
						where
							idorder='$idorder'
					");
					$querycek = $querycek->result();	
					$jml=0;
					foreach($querycek as $rows){
						$statuspembayaran=$rows->status;
						$jml++;
					}
					
					if($jml==0){
						$statuspembayaran="Belum Konfirmasi";
					}
			?>
				<table>
					<tr>
						<td>Nama Customer</td>
						<td>:</td>
						<td><?php echo $row->nama;?></td>
					</tr>
					<tr>
						<td>Telepon</td>
						<td>:</td>
						<td><?php echo $row->hp;?></td>
					</tr>
					<tr>
						<td>Email</td>
						<td>:</td>
						<td><?php echo $row->email;?></td>
					</tr>
					<tr>
						<td>Alamat</td>
						<td>:</td>
						<td><?php echo $row->alamat;?></td>
					</tr>
					<tr>
						<td>Status Alamat</td>
						<td>:</td>
						<td><?php echo $row->statusalamat;?></td>
					</tr>
					<tr>
						<td>Status Pemesanan</td>
						<td>:</td>
						<td><?php echo $row->status;?></td>
					</tr>
					<tr>
						<td>Status Konfirmasi Pembayaran</td>
						<td>:</td>
						<td><?php echo $statuspembayaran;?></td>
					</tr>
				</table>
			<?php
				}
			?>
			<h3>Detail Pemesanan</h3>
			<table width="100%" class="table table-striped table-bordered">
					<tr>
						<th>No</th>
						<th>ID Produk</th>
						<th>Nama Produk</th>
						<th>Harga</th>
						<th>Jumlah</th>
						<th>Sub Harga</th>
					</tr>
					<?php
						$no=1;
						$totalharga=0;
						foreach($query as $row){
					?>
					<tr>
						<td><?php echo"$no";?></td>
						<td><?php echo $row->idproduk;?></td>
						<td><?php echo $row->namaproduk;?></td>
						<td><?php echo $row->harga;?></td>
						<td><?php echo $row->jml;?></td>
						<td><?php echo $row->subharga;?></td>
					</tr>
					<?php
							$totalharga=$totalharga+$row->subharga;
							$no++;
						}
					?>
					<tr>
						<th colspan="5">Total Keseluruhan</th>
						<th><?php echo $totalharga;?></th>
					</tr>
				</table>
				
				<?php
					if($statusorder=="Terpesan" and $statuspembayaran=="Sudah Acc" and $_SESSION['levels']!="Customer"){
				?>
				<?php echo form_open('transaksi/mengirimbarang/'.$idorder); ?>
					<div style="text-align:right;"><input type="submit" name="submit" value="Kirim Barang"></div>
				<?php echo form_close(); ?>
				<?php
					}
				?>
			</p>
			</div>
		</div>
	  </div> <!-- /row -->
	</div> <!-- /container -->
  </div> <!-- /main-inner -->
</div>