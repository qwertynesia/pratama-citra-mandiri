<!DOCTYPE html>
<!-- Website template by freewebsitetemplates.com -->
<html>
<head>
	<title>APLIKASI PENJUALAN DAN PELAYANAN JASA KOMPUTER</title>
	<meta charset="utf-8">
	<link href="<?php echo base_url();?>lib/lib/css/style.css" rel="stylesheet" type="text/css">
	<!--[if IE 7]>
		<link href="css/ie7.css" rel="stylesheet" type="text/css">
	<![endif]-->
</head>
<body>
	<div id="background">
		<div id="page">
			<div id="header">
				<div style="margin-top:20px; text-align:center;"><?php echo anchor('home', 'APLIKASI PENJUALAN DAN PELAYANAN JASA KOMPUTER ', 'class="brand" style="color:#fff; text-decoration:none; font-size:20px;"');?></div>
				<ul class="navigation" style="margin-top:35px;">
				<?php
					if(empty($_SESSION['levels'])){
						$_SESSION['levels']="";
					}
					if(empty($_SESSION['levels']) or $_SESSION['levels']=="Customer"){
				?>
				  <li><a href="<?php echo base_url();?>index.php/customer/tentangkami">Tentang Kami</a></li>
				  <li><a href="<?php echo base_url();?>index.php/customer/hubungikami">Hubungi Kami</a></li>
				  <li><a href="<?php echo base_url();?>index.php/customer/faq">FAQ</a></li>
				<?php
						if(empty($_SESSION['levels'])){
				?>
				  <li><a href="<?php echo base_url();?>index.php/login/daftar">Daftar</a></li>
				  <li><a href="<?php echo base_url();?>index.php/login/login">Login</a></li>
				<?php
						}
					}if(!empty($_SESSION['levels'])){
				?>
				  <li><a href="<?php echo base_url();?>index.php/login/profil">Profile</a></li>
				  <li><a href="<?php echo base_url();?>index.php/login/logout">Logout</a></li>
				<?php
					}
				?>
				</ul>
			</div>
			<div id="body">
				<div id="section">
					<div class="sidebar">
						<h1 style="color:#fff;">Menu Utama</h1>
						<ul class="mainnav">
							<?php
								if(empty($_SESSION['levels']) or $_SESSION['levels']=="Customer"){
							?>
							<li><?php echo anchor('customer', '<i class="icon-dashboard"></i><span>Dashboard</span>');?></li>
							<?php
								}if($_SESSION['levels']=="Admin"){
							?>
							<li><?php echo anchor('home', '<i class="icon-dashboard"></i><span>Dashboard</span>');?></li>
							<li class="active"><?php echo anchor('#', '<i class="icon-list-alt"></i><span>Master</span> <b class="caret"></b>','class="dropdown-toggle" data-toggle="dropdown"');?>
								<ul class="dropdown-menu">
									<li><?php echo anchor('home/user', 'User');?></li>
									<li><?php echo anchor('home/customer', 'Customer');?></li>
									<li><?php echo anchor('home/kategoriproduk', 'Kategori Produk');?></li>
									<li><?php echo anchor('home/produk', 'Produk');?></li>
								</ul>
							</li>
							<li class="active"><?php echo anchor('#', '<i class="icon-list-alt"></i><span>Transaksi</span> <b class="caret"></b>','class="dropdown-toggle" data-toggle="dropdown"');?>
								<ul class="dropdown-menu">
									<li><?php echo anchor('transaksi/dataorder', 'Order');?></li>
									<li><?php echo anchor('transaksi/konfirmasibayar', 'Konfirmasi Pembayaran');?></li>
								</ul>
							</li>
							<?php
								}if(empty($_SESSION['levels']) or $_SESSION['levels']=="Customer"){
							?>
							<li class="active"><?php echo anchor('#', '<i class="icon-list-alt"></i><span>Kategori Produk</span> <b class="caret"></b>','class="dropdown-toggle" data-toggle="dropdown"');?>
								<ul class="dropdown-menu">
								<?php
									$this->load->database();
									$querycek = $this->db->query("
										select * from 
											kategoriproduk
									");
									$querycek = $querycek->result();	
									foreach($querycek as $row){
										$idkategoriproduk=$row->idkategoriproduk;
										$kategoriproduk=$row->kategoriproduk;
								?>
									<li><?php echo anchor('customer/produk/'.$idkategoriproduk, $kategoriproduk);?></li>
								<?php
									}
								?>
								</ul>
							</li>
							<?php
								}if($_SESSION['levels']=="Customer"){
							?>
							<li class="active"><?php echo anchor('#', '<i class="icon-list-alt"></i><span>Transaksi</span> <b class="caret"></b>','class="dropdown-toggle" data-toggle="dropdown"');?>
								<ul class="dropdown-menu">
									<li><?php echo anchor('transaksi/dataorder', 'Order');?></li>
									<li><?php echo anchor('transaksi/konfirmasibayar', 'Konfirmasi Pembayaran');?></li>
									<li><?php echo anchor('transaksi/progress', 'Progress');?></li>
								</ul>
							</li>
							<?php
								}
							?>
						</ul>
					</div>
					<div class="content">