<div class="main">
  <div class="main-inner">
	<div class="container">
	   <div class="row all-icons">    
		<div class="widget">
			<div class="widget-header">
				<i class="icon-list-alt"></i>
				<h3>Selamat Datang</h3>
			</div>
			<!-- /widget-header -->
			<div class="widget-content">
			<p>
				Jangan lupa logout setelah menggunakan halaman ini.
			</p>
			</div>
		</div>
	  </div> <!-- /row -->
	</div> <!-- /container -->
  </div> <!-- /main-inner -->
</div>