<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <title>APLIKASI PENJUALAN DAN PELAYANAN JASA KOMPUTER</title>

	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes"> 
    
	<link href="<?php echo base_url();?>lib/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
	<link href="<?php echo base_url();?>lib/css/bootstrap-responsive.min.css" rel="stylesheet" type="text/css" />

	<link href="<?php echo base_url();?>lib/css/font-awesome.css" rel="stylesheet">
	<link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
		
	<link href="<?php echo base_url();?>lib/css/style.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url();?>lib/css/pages/signin.css" rel="stylesheet" type="text/css">
	
	<script src="<?php echo base_url();?>lib/js/jquery-1.7.2.min.js"></script>
	<script src="<?php echo base_url();?>lib/js/bootstrap.js"></script>

	<script src="<?php echo base_url();?>lib/js/signin.js"></script>
</head>

<body>
<div class="account-container">
	<div class="content clearfix">
		<?php echo form_open('login/ceklogin'); ?>
			<h1>Login</h1>
			<div class="login-fields">
				
				<?php
					$err = $this->input->get('err');
					if($err==1){
						echo"<font color='#ff0000'>Username dan Password Anda salah.</font>";
					}else if($err==2){
						echo"<font color='#6A9B25'>Anda berhasil logout.</font>";
					}else if($err==3){
						echo"<font color='#ff0000'>Silahkan Masuk untuk mengakses halaman ini.</font>";
					}else if($err==4){
						echo"<font color='#6A9B25'>Terima Kasih Telah mendaftar.</font>";
					}else if($err==5){
						echo"<font color='#ff0000'>Silahkan Login untuk memesan Produk.</font>";
					}
				?>
				
				<p>Silahkan Masukkan username dan password Anda : </p>
				
				<div class="field" style="margin-top:5px;">
					<label for="username">Username</label>
					<input type="text" id="username" name="username" value="" placeholder="Username" class="login username-field" />
				</div> <!-- /field -->
				
				<div class="field">
					<label for="password">Password:</label>
					<input type="password" id="password" name="password" value="" placeholder="Password" class="login password-field"/>
				</div> <!-- /password -->
				
			</div> <!-- /login-fields -->
			
			<div class="login-actions">
									
				<button class="button btn btn-success btn-large" style="background:#000f00;">Login</button>
				
			</div> <!-- .actions -->
		<?php echo form_close(); ?>
		<?php echo form_open('customer'); ?>
			<button class="button btn btn-success btn-large" style="background:#000f00; margin-top:-52px; margin-right:70px;">Back To Home</button>
		<?php echo form_close(); ?>
	</div> <!-- /content -->
</div> <!-- /account-container -->
</body>
</html>