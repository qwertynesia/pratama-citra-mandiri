<div class="main">
  <div class="main-inner">
	<div class="container">
	   <div class="row all-icons">    
		<div class="widget">
			<div class="widget-header">
				<i class="icon-list-alt"></i>
				<h3>Edit Customer</h3>
			</div>
			<!-- /widget-header -->
			<div class="widget-content">
			<p>
				<?php echo form_open('home/customereditact/'.$p); ?>
				<?php
					foreach($query as $row){
				?>
				<table>
					<tr>
						<td>Nama</td>
						<td>:</td>
						<td><input type="text" name="nama" value="<?php echo $row->nama;?>" required></td>
					</tr>
					<tr>
						<td>Jenis Kelamin</td>
						<td>:</td>
						<td>
						<select name="jk" required>
							<option value="<?php echo $row->jk;?>"><?php echo $row->jk;?></option>
							<option value="Laki - laki">Laki - laki</option>
							<option value="Perempuan">Perempuan</option>
						</select>
						</td>
					</tr>
					<tr>
						<td>Hp</td>
						<td>:</td>
						<td><input type="text" name="hp" value="<?php echo $row->hp;?>" required></td>
					</tr>
					<tr>
						<td>Email</td>
						<td>:</td>
						<td><input type="text" name="email" value="<?php echo $row->email;?>" required></td>
					</tr>
					<tr>
						<td>Alamat</td>
						<td>:</td>
						<td><textarea name="alamat" cols="30" rows="5" required><?php echo $row->alamat;?></textarea></td>
					</tr>
					<tr>
						<td>Username</td>
						<td>:</td>
						<td><input type="text" name="user" value="<?php echo $row->user;?>" required></td>
					</tr>
					<tr>
						<td>Password</td>
						<td>:</td>
						<td><input type="password" name="pass" value=""></td>
					</tr>
					<tr>
						<td colspan="3"><input type="submit" name="submit" value="Edit"></td>
					</tr>
				</table>
				<?php
					}
				?>
				<?php echo form_close(); ?>
			</p>
			</div>
		</div>
	  </div> <!-- /row -->
	</div> <!-- /container -->
  </div> <!-- /main-inner -->
</div>