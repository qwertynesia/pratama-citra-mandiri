<div class="main">
  <div class="main-inner">
	<div class="container">
	   <div class="row all-icons">    
		<div class="widget">
			<div class="widget-header">
				<i class="icon-list-alt"></i>
				<h3>Edit User</h3>
			</div>
			<!-- /widget-header -->
			<div class="widget-content">
			<p>
				<?php echo form_open('home/usereditact/'.$p); ?>
				<?php
					foreach($query as $row){
				?>
				<table>
					<tr>
						<td>Nama</td>
						<td>:</td>
						<td><input type="text" name="nama" value="<?php echo $row->nama;?>" required></td>
					</tr>
					<tr>
						<td>Telepon</td>
						<td>:</td>
						<td><input type="number" name="tlp" value="<?php echo $row->tlp;?>" required></td>
					</tr>
					<tr>
						<td>Level</td>
						<td>:</td>
						<td>
						<select name="level" required>
							<option value="<?php echo $row->level;?>"><?php echo $row->level;?></option>
							<option value="Admin">Admin</option>
							<option value="User">User</option>
						</select>
						</td>
					</tr>
					<tr>
						<td>Username</td>
						<td>:</td>
						<td><input type="text" name="user" value="<?php echo $row->username;?>" required></td>
					</tr>
					<tr>
						<td>Password</td>
						<td>:</td>
						<td><input type="password" name="pass" value=""></td>
					</tr>
					<tr>
						<td colspan="3"><input type="submit" name="submit" value="Edit"></td>
					</tr>
				</table>
				<?php
					}
				?>
				<?php echo form_close(); ?>
			</p>
			</div>
		</div>
	  </div> <!-- /row -->
	</div> <!-- /container -->
  </div> <!-- /main-inner -->
</div>