<div class="main">
  <div class="main-inner">
	<div class="container">
	   <div class="row all-icons">    
		<div class="widget">
			<div class="widget-header">
				<i class="icon-list-alt"></i>
				<h3>Edit Produk</h3>
			</div>
			<!-- /widget-header -->
			<div class="widget-content">
			<p>
				<?php 
					$attributes = array('onsubmit' => 'return validate_form ();', 'name' => 'tambah');
					echo form_open_multipart('home/produkeditact/'.$p, $attributes);
				?>
				<?php
					foreach($query as $row){
				?>
				<table>
					<tr>
						<td>Kode Produk</td>
						<td>:</td>
						<td><input type="text" name="idproduk" value="<?php echo $row->idproduk;?>" readonly></td>
					</tr>
					<tr>
						<td>Nama Produk</td>
						<td>:</td>
						<td><input type="text" name="namaproduk" value="<?php echo $row->namaproduk;?>" required></td>
					</tr>
					<tr>
						<td>Kategori Produk</td>
						<td>:</td>
						<td>
						<select name="idkategoriproduk" required>
							<option value="<?php echo $row->idkategoriproduk;?>"><?php echo $row->kategoriproduk;?></option>
						<?php
							foreach($kategoriproduk as $rows){
						?>
							<option value="<?php echo $rows->idkategoriproduk;?>"><?php echo $rows->kategoriproduk;?></option>
						<?php
							}
						?>
						</select>
						</td>
					</tr>
					<tr>
						<td>Deskripsi</td>
						<td>:</td>
						<td><textarea name="deskripsi" cols="30" rows="5" required><?php echo $row->deskripsi;?></textarea></td>
					</tr>
					<tr>
						<td>Gambar</td>
						<td>:</td>
						<td><input type="file" name="gambar" value=""></td>
					</tr>
					<tr>
						<td>Harga</td>
						<td>:</td>
						<td><input type="number" name="harga" value="<?php echo $row->harga;?>" required></td>
					</tr>
					<tr>
						<td>Stok</td>
						<td>:</td>
						<td><input type="number" name="stok" value="<?php echo $row->stok;?>" required></td>
					</tr>
					<tr>
						<td colspan="3"><input type="submit" name="submit" value="Edit"></td>
					</tr>
				</table>
				<?php
					}
				?>
				<?php echo form_close(); ?>
			</p>
			</div>
		</div>
	  </div> <!-- /row -->
	</div> <!-- /container -->
  </div> <!-- /main-inner -->
</div>