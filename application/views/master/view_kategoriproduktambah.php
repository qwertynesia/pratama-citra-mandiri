<div class="main">
  <div class="main-inner">
	<div class="container">
	   <div class="row all-icons">    
		<div class="widget">
			<div class="widget-header">
				<i class="icon-list-alt"></i>
				<h3>Tambah Kategori Produk</h3>
			</div>
			<!-- /widget-header -->
			<div class="widget-content">
			<p>
				<?php echo form_open('home/kategoriproduktambahact'); ?>
				<table>
					<tr>
						<td>Kategori Produk</td>
						<td>:</td>
						<td><input type="text" name="kategoriproduk" value="" required></td>
					</tr>
					<tr>
						<td colspan="3"><input type="submit" name="submit" value="Simpan"></td>
					</tr>
				</table>
				<?php echo form_close(); ?>
			</p>
			</div>
		</div>
	  </div> <!-- /row -->
	</div> <!-- /container -->
  </div> <!-- /main-inner -->
</div>