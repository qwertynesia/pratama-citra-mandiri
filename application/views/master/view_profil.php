<div class="main">
  <div class="main-inner">
	<div class="container">
	   <div class="row all-icons">    
		<div class="widget">
			<div class="widget-header">
				<i class="icon-list-alt"></i>
				<h3>Profil</h3>
			</div>
			<!-- /widget-header -->
			<div class="widget-content">
			<p>
				<table width="100%" class="table table-striped table-bordered">
					<?php
						foreach($query as $row){
					?>
					<tr>
						<th>Nama</th>
						<th>:</th>
						<td><?php echo $row->nama;?></td>
					</tr>
					<?php
						if($_SESSION['levels']=="Customer"){
					?>
					<tr>
						<th>Jenis Kelamin</th>
						<th>:</th>
						<td><?php echo $row->jk;?></td>
					</tr>
					<?php
						}
					?>
					<tr>
						<th>Telepon</th>
						<th>:</th>
						<td><?php echo $row->hp;?></td>
					</tr>
					<?php
						if($_SESSION['levels']=="Customer"){
					?>
					<tr>
						<th>Email</th>
						<th>:</th>
						<td><?php echo $row->email;?></td>
					</tr>
					<tr>
						<th>Alamat</th>
						<th>:</th>
						<td><?php echo $row->alamat;?></td>
					</tr>
					<?php
						}
					?>
					<tr>
						<th>Username</th>
						<th>:</th>
						<td><?php echo $row->user;?></td>
					</tr>
					<tr>
						<th>Status</th>
						<th>:</th>
						<td><?php echo $row->status;?></td>
					</tr>
					<?php
						}
					?>
				</table>
			</p>
			</div>
		</div>
	  </div> <!-- /row -->
	</div> <!-- /container -->
  </div> <!-- /main-inner -->
</div>