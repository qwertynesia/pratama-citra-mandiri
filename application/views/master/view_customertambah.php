<div class="main">
  <div class="main-inner">
	<div class="container">
	   <div class="row all-icons">    
		<div class="widget">
			<div class="widget-header">
				<i class="icon-list-alt"></i>
				<h3>Tambah Customer</h3>
			</div>
			<!-- /widget-header -->
			<div class="widget-content">
			<p>
				<?php echo form_open('home/customertambahact/1'); ?>
				<table>
					<tr>
						<td>Nama</td>
						<td>:</td>
						<td><input type="text" name="nama" value="" required></td>
					</tr>
					<tr>
						<td>Jenis Kelamin</td>
						<td>:</td>
						<td>
						<select name="jk" required>
							<option value="Laki - laki">Laki - laki</option>
							<option value="Perempuan">Perempuan</option>
						</select>
						</td>
					</tr>
					<tr>
						<td>Hp</td>
						<td>:</td>
						<td><input type="text" name="hp" value="" required></td>
					</tr>
					<tr>
						<td>Email</td>
						<td>:</td>
						<td><input type="text" name="email" value="" required></td>
					</tr>
					<tr>
						<td>Alamat</td>
						<td>:</td>
						<td><textarea name="alamat" cols="30" rows="5" required></textarea></td>
					</tr>
					<tr>
						<td>Username</td>
						<td>:</td>
						<td><input type="text" name="user" value="" required></td>
					</tr>
					<tr>
						<td>Password</td>
						<td>:</td>
						<td><input type="password" name="pass" value="" required></td>
					</tr>
					<tr>
						<td colspan="3"><input type="submit" name="submit" value="Simpan"></td>
					</tr>
				</table>
				<?php echo form_close(); ?>
			</p>
			</div>
		</div>
	  </div> <!-- /row -->
	</div> <!-- /container -->
  </div> <!-- /main-inner -->
</div>