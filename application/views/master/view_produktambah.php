<div class="main">
  <div class="main-inner">
	<div class="container">
	   <div class="row all-icons">    
		<div class="widget">
			<div class="widget-header">
				<i class="icon-list-alt"></i>
				<h3>Tambah Produk</h3>
			</div>
			<!-- /widget-header -->
			<div class="widget-content">
			<p>
				<?php 
					$attributes = array('onsubmit' => 'return validate_form ();', 'name' => 'tambah');
					echo form_open_multipart('home/produktambahact', $attributes);
				?>
				<table>
					<tr>
						<td>Kode Produk</td>
						<td>:</td>
						<td><input type="text" name="idproduk" value="" required></td>
					</tr>
					<tr>
						<td>Nama Produk</td>
						<td>:</td>
						<td><input type="text" name="namaproduk" value="" required></td>
					</tr>
					<tr>
						<td>Kategori Produk</td>
						<td>:</td>
						<td>
						<select name="idkategoriproduk" required>
						<?php
							foreach($kategoriproduk as $row){
						?>
							<option value="<?php echo $row->idkategoriproduk;?>"><?php echo $row->kategoriproduk;?></option>
						<?php
							}
						?>
						</select>
						</td>
					</tr>
					<tr>
						<td>Deskripsi</td>
						<td>:</td>
						<td><textarea name="deskripsi" cols="30" rows="5" required></textarea></td>
					</tr>
					<tr>
						<td>Gambar</td>
						<td>:</td>
						<td><input type="file" name="gambar" value="" required></td>
					</tr>
					<tr>
						<td>Harga</td>
						<td>:</td>
						<td><input type="number" name="harga" value="" required></td>
					</tr>
					<tr>
						<td>Stok</td>
						<td>:</td>
						<td><input type="number" name="stok" value="" required></td>
					</tr>
					<tr>
						<td colspan="3"><input type="submit" name="submit" value="Simpan"></td>
					</tr>
				</table>
				<?php echo form_close(); ?>
			</p>
			</div>
		</div>
	  </div> <!-- /row -->
	</div> <!-- /container -->
  </div> <!-- /main-inner -->
</div>