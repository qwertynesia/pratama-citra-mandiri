<div class="main">
  <div class="main-inner">
	<div class="container">
	   <div class="row all-icons">    
		<div class="widget">
			<div class="widget-header">
				<i class="icon-list-alt"></i>
				<h3>Edit Kategori Produk</h3>
			</div>
			<!-- /widget-header -->
			<div class="widget-content">
			<p>
				<?php echo form_open('home/kategoriprodukeditact/'.$p); ?>
				<?php
					foreach($query as $row){
				?>
				<table>
					<tr>
						<td>Kategori Produk</td>
						<td>:</td>
						<td><input type="text" name="kategoriproduk" value="<?php echo $row->kategoriproduk;?>" required></td>
					</tr>
					<tr>
						<td colspan="3"><input type="submit" name="submit" value="Edit"></td>
					</tr>
				</table>
				<?php
					}
				?>
				<?php echo form_close(); ?>
			</p>
			</div>
		</div>
	  </div> <!-- /row -->
	</div> <!-- /container -->
  </div> <!-- /main-inner -->
</div>