<div class="main">
  <div class="main-inner">
	<div class="container">
	   <div class="row all-icons">    
		<div class="widget">
			<div class="widget-header">
				<i class="icon-list-alt"></i>
				<h3>Data Customer</h3>
			</div>
			<!-- /widget-header -->
			<div class="widget-content">
			<p>
				<?php echo anchor('home/customertambah', 'Tambah');?>
				<?php echo form_open('home/customer'); ?>
					<table>
						<tr>
							<td>Cari</td>
							<td>:</td>
							<td><input type="text" name="cari" value="" onchange="submit()" required></td>
						</tr>
					</table>
				<?php echo form_close(); ?>
				<table width="100%" class="table table-striped table-bordered">
					<tr>
						<th>No</th>
						<th>Nama</th>
						<th>Jenis Kelamin</th>
						<th>Telepon</th>
						<th>Email</th>
						<th>Alamat</th>
						<th colspan="2"><center>Aksi</center></th>
					</tr>
					<?php
						$no=1;
						if(empty($offset)){
							$no=1;
						}else{
							$no=$no+$offset;
						}
						$jml=0;
						foreach($query->result() as $row){
					?>
					<tr>
						<td><?php echo"$no";?></td>
						<td><?php echo $row->nama;?></td>
						<td><?php echo $row->jk;?></td>
						<td><?php echo $row->hp;?></td>
						<td><?php echo $row->email;?></td>
						<td><?php echo $row->alamat;?></td>
						<td><center><?php echo anchor('home/customeredit/'.$row->idcustomer, 'Edit');?></center></td>
						<td><center><?php echo anchor('home/customerhapus/'.$row->idcustomer, 'Hapus');?></center></td>
					</tr>
					<?php
							$no++;
						}
					?>
				</table>
				<p align="center"><?php echo $this->pagination->create_links();?></p>
			</p>
			</div>
		</div>
	  </div> <!-- /row -->
	</div> <!-- /container -->
  </div> <!-- /main-inner -->
</div>