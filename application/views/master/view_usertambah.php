<div class="main">
  <div class="main-inner">
	<div class="container">
	   <div class="row all-icons">    
		<div class="widget">
			<div class="widget-header">
				<i class="icon-list-alt"></i>
				<h3>Tambah User</h3>
			</div>
			<!-- /widget-header -->
			<div class="widget-content">
			<p>
				<?php echo form_open('home/usertambahact'); ?>
				<table>
					<tr>
						<td>Nama</td>
						<td>:</td>
						<td><input type="text" name="nama" value="" required></td>
					</tr>
					<tr>
						<td>Telepon</td>
						<td>:</td>
						<td><input type="text" name="tlp" value="" required></td>
					</tr>
					<tr>
						<td>Level</td>
						<td>:</td>
						<td>
						<select name="level" required>
							<option value="Admin">Admin</option>
							<option value="User">User</option>
						</select>
						</td>
					</tr>
					<tr>
						<td>Username</td>
						<td>:</td>
						<td><input type="text" name="user" value="" required></td>
					</tr>
					<tr>
						<td>Password</td>
						<td>:</td>
						<td><input type="password" name="pass" value="" required></td>
					</tr>
					<tr>
						<td colspan="3"><input type="submit" name="submit" value="Simpan"></td>
					</tr>
				</table>
				<?php echo form_close(); ?>
			</p>
			</div>
		</div>
	  </div> <!-- /row -->
	</div> <!-- /container -->
  </div> <!-- /main-inner -->
</div>