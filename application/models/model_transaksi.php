<?php
class Model_transaksi extends CI_Controller {
	function _construct() {
	parent::CI_Controller();
}

	function dataorder() {
		$this->load->database();
		$this->load->library('pagination');
		$cek="1=1";
		$idp=$_SESSION['idp'];
		if($_SESSION['levels']=="Customer"){
			$cek=" a.idcustomer='$idp'";
			
		}
		if(empty($_POST['cari'])){
			$string_query = "
				select *,a.status from 
					dataorder a,
					customer b
				where 
					a.idcustomer=b.idcustomer and
					$cek and
					a.status!='keranjang'
			";
		}else{
			$cari=$_POST['cari'];
			$string_query = "
				select *,a.status from 
					dataorder a,
					customer b
				where 
					a.idcustomer=b.idcustomer and
					(a.tgl like '%$cari%' or 
					a.idorder like '%$cari%') and
					$cek and
					a.status!='Keranjang'
			";
		}
        $query = $this->db->query($string_query); 
		$config['base_url']     = base_url().'index.php/transaksi/dataorder/';  
        $config['total_rows']   = $query->num_rows();  
        $config['per_page']     = '5';  
        $num            = $config['per_page'];  
        $offset         = $this->uri->segment(3);  
        $offset         = ( ! is_numeric($offset) || $offset < 1) ? 0 : $offset;  
          
        if(empty($offset))  
        {  
            $offset=0;  
        }  
          
        $this->pagination->initialize($config);         
          
        $data['offset']      = $offset;    
        $data['query']      = $this->db->query($string_query." limit $offset,$num");    
        $data['base']       = $this->config->item('base_url');  
      
        return $data;
	}
	
	function orderdetail($p) {
		$this->load->database();
		$querycek = $this->db->query("
			select * from 
				detailorder a,
				produk b
			where
				a.idproduk=b.idproduk and
				a.idorder='$p'
		");
		return $querycek->result();	
	}
	
	function orderpemesan($p) {
		$this->load->database();
		$querycek = $this->db->query("
			select *,a.status from 
				dataorder a,
				customer b
			where
				a.idcustomer=b.idcustomer and
				a.idorder='$p' and
				a.status!='Keranjang'
		");
		return $querycek->result();	
	}
	
	function konfirmasibayar() {
		$this->load->database();
		$this->load->library('pagination');
		$cek="1=1";
		$idp=$_SESSION['idp'];
		if($_SESSION['levels']=="Customer"){
			$cek=" a.idcustomer='$idp'";
			
		}
		if(empty($_POST['cari'])){
			$string_query = "
				select b.* from 
					dataorder a,
					konfirmasipembayaran b
				where
					a.idorder=b.idorder and
					$cek
			";
		}else{
			$cari=$_POST['cari'];
			$string_query = "
				select b.* from 
					dataorder a,
					konfirmasipembayaran b
				where
					a.idorder=b.idorder and
					(b.tgl like '%$cari%' or 
					b.idorder like '%$cari%') and
					$cek
			";
		}
        $query = $this->db->query($string_query); 
		$config['base_url']     = base_url().'index.php/transaksi/konfirmasibayar/';  
        $config['total_rows']   = $query->num_rows();  
        $config['per_page']     = '5';  
        $num            = $config['per_page'];  
        $offset         = $this->uri->segment(3);  
        $offset         = ( ! is_numeric($offset) || $offset < 1) ? 0 : $offset;  
          
        if(empty($offset))  
        {  
            $offset=0;  
        }  
          
        $this->pagination->initialize($config);         
          
        $data['offset']      = $offset;    
        $data['query']      = $this->db->query($string_query." limit $offset,$num");    
        $data['base']       = $this->config->item('base_url');  
      
        return $data;
	}
	
	function keranjang($p,$q) {
		$this->load->database();
		$idp=$_SESSION['idp'];
		if($q==1){
			$querycek = $this->db->query("
				select * from 
					dataorder
				where
					idcustomer='$idp' and 
					status='Keranjang'
			");
			if($querycek->num_rows()==0){
				//cek id order
				$querycek = $this->db->query("
					select * from 
						dataorder
					order by
						idorder asc
				");
				$querycek = $querycek->result();	
				foreach($querycek as $row){
					$idorder=$row->idorder;
				}
				$idorder=explode("OR",$idorder);
				$idorder=$idorder[1]+1;
				if($idorder<=9){
					$idorder="OR00".$idorder;
				}elseif($idorder<=99){
					$idorder="OR0".$idorder;
				}elseif($idorder<9){
					$idorder="OR".$idorder;
				}
				
				//cek data produk
				$querycek = $this->db->query("
					select * from 
						produk
					where
						idproduk='$p'
				");
				$querycek = $querycek->result();	
				$totalharga=0;
				$totalkeseluruhan=0;
				$biayakirim=0;
				$tgl=date("Y-m-d");
				foreach($querycek as $row){
					$idproduk=$row->idproduk;
					$harga=$row->harga;
					$jml=1;
					$subharga=$harga*$jml;
					$totalharga=$totalharga+$subharga;
				}
				$totalkeseluruhan=$totalharga+$biayakirim;
				
				//insert data order
				$querycek = $this->db->query("
					insert into dataorder values(
						'$idorder',
						'$idp',
						'0',
						'$totalharga',
						'$biayakirim',
						'$totalkeseluruhan',
						'$tgl',
						'Alamat 1',
						'Keranjang'
					)
				");
				
				//insert detail order
				$querycek = $this->db->query("
					insert into detailorder values(
						NULL,
						'$idorder',
						'$idproduk',
						'$harga',
						'$jml',
						'$subharga'
					)
				");
				
				$status=1;
			}else{
				//cek id order
				$querycek = $querycek->result();	
				foreach($querycek as $row){
					$idorder=$row->idorder;
				}
				
				//cek data produk
				$querycek = $this->db->query("
					select * from 
						produk
					where
						idproduk='$p'
				");
				$querycek = $querycek->result();	
				$totalharga=0;
				$totalkeseluruhan=0;
				$biayakirim=0;
				$tgl=date("Y-m-d");
				foreach($querycek as $row){
					$idproduk=$row->idproduk;
					$harga=$row->harga;
					$jml=1;
					$subharga=$harga*$jml;
				}
				
				//cek keberadaan data detail prduk
				$querycek = $this->db->query("
					select * from 
						detailorder
					where
						idproduk='$p' and
						idorder='$idorder'
				");
				if($querycek->num_rows()==0){
				
					//insert detail order
					$querycek = $this->db->query("
						insert into detailorder values(
							NULL,
							'$idorder',
							'$idproduk',
							'$harga',
							'$jml',
							'$subharga'
						)
					");
					
					//cek total harga detail produk
					$querycek = $this->db->query("
						select sum(subharga) as totalharga from 
							detailorder
						where
							idorder='$idorder'
						group by
							idorder
					");
					$querycek = $querycek->result();	
					foreach($querycek as $row){
						$totalharga=$row->totalharga;
					}
					
					//cek total biaya kirim
					$querycek = $this->db->query("
						select sum(biayakirim) as biayakirim from 
							dataorder
						where
							idorder='$idorder'
						group by
							idorder
					");
					$querycek = $querycek->result();	
					foreach($querycek as $row){
						$biayakirim=$row->biayakirim;
					}
					
					$totalkeseluruhan=$totalharga+$biayakirim;
					
					//update data order
					$querycek = $this->db->query("
						update
							dataorder
						set
							subtotal='$totalharga',
							biayakirim='$biayakirim',
							total='$totalkeseluruhan'
						where
							idorder='$idorder'
					");
					$status=1;
				}else{
					$status=2;
				}
			}
		}elseif($q==2){
			$idorder=$_POST['idorder'];
			$submit=$_POST['submit'];
			if($submit=="Ubah"){
				//cek data produk
				$querycek = $this->db->query("
					select * from 
						produk
					where
						idproduk='$p'
				");
				$querycek = $querycek->result();	
				$totalharga=0;
				$totalkeseluruhan=0;
				$biayakirim=0;
				$tgl=date("Y-m-d");
				foreach($querycek as $row){
					$idproduk=$row->idproduk;
					$harga=$row->harga;
					$jml=$_POST['jml'];
					$subharga=$harga*$jml;
				}
				
				//update detail order
				$querycek = $this->db->query("
					update
						detailorder
					set
						harga='$harga',
						jml='$jml',
						subharga='$subharga'
					where
						idproduk='$p' and
						idorder='$idorder'
				");
				
				//cek total harga detail produk
				$querycek = $this->db->query("
					select sum(subharga) as totalharga from 
						detailorder
					where
						idorder='$idorder'
					group by
						idorder
				");
				$querycek = $querycek->result();	
				foreach($querycek as $row){
					$totalharga=$row->totalharga;
				}
				
				//cek total biaya kirim
				$querycek = $this->db->query("
					select sum(biayakirim) as biayakirim from 
						dataorder
					where
						idorder='$idorder'
					group by
						idorder
				");
				$querycek = $querycek->result();	
				foreach($querycek as $row){
					$biayakirim=$row->biayakirim;
				}
				
				$totalkeseluruhan=$totalharga+$biayakirim;
				
				//update data order
				$querycek = $this->db->query("
					update
						dataorder
					set
						subtotal='$totalharga',
						biayakirim='$biayakirim',
						total='$totalkeseluruhan'
					where
						idorder='$idorder'
				");
				$status=3;
			}elseif($submit=="Hapus"){
				//hapus detail order
				$querycek = $this->db->query("
					delete from
						detailorder
					where
						idproduk='$p' and
						idorder='$idorder'
				");
				
				//cek total harga detail produk
				$querycek = $this->db->query("
					select sum(subharga) as totalharga from 
						detailorder
					where
						idorder='$idorder'
					group by
						idorder
				");
				$querycek = $querycek->result();	
				foreach($querycek as $row){
					$totalharga=$row->totalharga;
				}
				
				//cek total biaya kirim
				$querycek = $this->db->query("
					select sum(biayakirim) as biayakirim from 
						dataorder
					where
						idorder='$idorder'
					group by
						idorder
				");
				$querycek = $querycek->result();	
				foreach($querycek as $row){
					$biayakirim=$row->biayakirim;
				}
				
				$totalkeseluruhan=$totalharga+$biayakirim;
				
				//update data order
				$querycek = $this->db->query("
					update
						dataorder
					set
						subtotal='$totalharga',
						biayakirim='$biayakirim',
						total='$totalkeseluruhan'
					where
						idorder='$idorder'
				");
				$status=3;
				
				$status=4;
			}
		}
		return $status;
	}
	
	function keranjangproduk() {
		$this->load->database();
		$idp=$_SESSION['idp'];
		$querycek = $this->db->query("
			select c.*,d.*,a.status from 
				dataorder a,
				customer b,
				detailorder c,
				produk d
			where 
				a.idcustomer=b.idcustomer and
				a.idcustomer='$idp' and
				a.idorder=c.idorder and
				c.idproduk=d.idproduk and
				a.status='Keranjang'
		");
		return $querycek->result();	
	}
	
	function checkout($p) {
		$this->load->database();
		$querycek = $this->db->query("
			select * from 
				detailorder a,
				produk b
			where
				a.idproduk=b.idproduk and
				a.idorder='$p'
		");
		return $querycek->result();	
	}
	
	function checkoutpemesan($p) {
		$this->load->database();
		$querycek = $this->db->query("
			select *,a.status from 
				dataorder a,
				customer b
			where
				a.idcustomer=b.idcustomer and
				a.idorder='$p' and
				a.status='Keranjang'
		");
		return $querycek->result();	
	}
	
	function pesan($p) {
		$this->load->database();
		$querycek = $this->db->query("
			select * from 
				dataorder a,
				customer b
			where
				a.idcustomer=b.idcustomer and
				a.idorder='$p'
		");
		$querycek = $querycek->result();	
		foreach($querycek as $row){
			$hp=$row->hp;
		}
		
		$pesan="Terima Kasih Telah memesan. Id order Anda : $p. Kami menunggu konfirmasi Pembayaran Anda.";
		$querycek = $this->db->query("
			insert into outbox (
				DestinationNumber,
				TextDecoded
			)
			values (
				'$hp',
				'$pesan'
			)
		");
		
		$querycek = $this->db->query("
			update dataorder set
				status='Terpesan'
			where
				idorder='$p'
		");
	}
	
	function mengirimbarang($p) {
		$this->load->database();
		$querycek = $this->db->query("
			update dataorder set
				status='Terkirim'
			where
				idorder='$p'
		");
	}
	
	function konfirmasi($p) {
		$this->load->database();
		$querycek = $this->db->query("
			update konfirmasipembayaran set
				status='Sudah Acc'
			where
				idkonfirmasipembayaran='$p'
		");
	}
	
	function dataorderpilihan() {
		$this->load->database();
		$idp=$_SESSION['idp'];
		$querycek = $this->db->query("
			select a.* from 
				(
					select * from
						dataorder
					where
						idcustomer='$idp' and
						status='Terpesan'
				) a
			left join
				konfirmasipembayaran b
			on
				(a.idorder=b.idorder)
			where
				b.idorder is NULL
		");
		return $querycek->result();	
	}
	
	function dataorderpilihan2() {
		$this->load->database();
		$idp=$_SESSION['idp'];
		$idorder=$_POST['idorder'];
		$querycek = $this->db->query("
			select * from 
				dataorder
			where
				idorder='$idorder'
		");
		return $querycek->result();	
	}
	
	function konfirmasibayartambah() {
		$this->load->database();
		$idp=$_SESSION['idp'];
		$idorder=$_POST['idorder'];
		$namapengirim=$_POST['namapengirim'];
		$norek=$_POST['norek'];
		$bank=$_POST['bank'];
		$jmltransfer=$_POST['jmltransfer'];
		$buktitransfer=$_POST['buktitransfer'];
		$tgl=date("Y-m-d");
		$querycek = $this->db->query("
			insert into konfirmasipembayaran values(
				NULL,
				'$idorder',
				'$namapengirim',
				'$norek',
				'$bank',
				'$jmltransfer',
				'$buktitransfer',
				'$tgl',
				'Belum Acc'
			)
		");
	}
	
}
 
?>