<?php
class Model_home extends CI_Controller {
	function _construct() {
	parent::CI_Controller();
}
	
	function pilihkategoriproduk() {
		$this->load->database();
		$querycek = $this->db->query("
			select * from 
				kategoriproduk
		");
		return $querycek->result();	
	}
	
	function user() {
		$this->load->database();
		$this->load->library('pagination');
		if(empty($_POST['cari'])){
			$string_query = "
				select * from 
					admin
			";
		}else{
			$cari=$_POST['cari'];
			$string_query = "
				select * from 
					admin
				where 
					(nama like '%$cari%' or 
					tlp like '%$cari%')
			";
		}
        $query = $this->db->query($string_query); 
		$config['base_url']     = base_url().'index.php/home/user/';  
        $config['total_rows']   = $query->num_rows();  
        $config['per_page']     = '5';  
        $num            = $config['per_page'];  
        $offset         = $this->uri->segment(3);  
        $offset         = ( ! is_numeric($offset) || $offset < 1) ? 0 : $offset;  
          
        if(empty($offset))  
        {  
            $offset=0;  
        }  
          
        $this->pagination->initialize($config);         
          
        $data['offset']      = $offset;    
        $data['query']      = $this->db->query($string_query." limit $offset,$num");    
        $data['base']       = $this->config->item('base_url');  
      
        return $data;
	}
	
	function usertambah() {
		$this->load->database();
		$nama=$_POST['nama'];
		$tlp=$_POST['tlp'];
		$user=$_POST['user'];
		$pass=$_POST['pass'];
		$pass=md5("$pass");
		$level=$_POST['level'];
		$querycek = $this->db->query("
			insert into admin values (
				null,
				'$nama',
				'$tlp',
				'$user',
				'$pass',
				'$level',
				'Aktif'
			)
		");
	}
	
	function useredit($p) {
		$this->load->database();
		$querycek = $this->db->query("
			select * from 
				admin
			where 
				idadmin='$p'
		");
		return $querycek->result();	
	}
	
	function usereditact($p) {
		$this->load->database();
		$nama=$_POST['nama'];
		$tlp=$_POST['tlp'];
		$user=$_POST['user'];
		$level=$_POST['level'];
		$querycek = $this->db->query("
			update admin set 
				nama='$nama',
				tlp='$tlp',
				level='$level',
				username='$user' 
			where 
				idadmin='$p'
		");
		if(!empty($_POST['pass'])){
			$pass=$_POST['pass'];
			$pass=md5("$pass");
			$querycek = $this->db->query("
				update admin set 
					password='$pass' 
				where 
					idadmin='$p'
			");
		}
	}
	
	function userhapus($p) {
		$this->load->database();
		$querycek = $this->db->query("delete from admin where idadmin='$p'");
	}
	
	function customer() {
		$this->load->database();
		$this->load->library('pagination');
		if(empty($_POST['cari'])){
			$string_query = "
				select * from 
					customer
			";
		}else{
			$cari=$_POST['cari'];
			$string_query = "
				select * from 
					customer
				where
					(nama like '%$cari%' or 
					hp like '%$cari%')
			";
		}
        $query = $this->db->query($string_query); 
		$config['base_url']     = base_url().'index.php/home/customer/';  
        $config['total_rows']   = $query->num_rows();  
        $config['per_page']     = '5';  
        $num            = $config['per_page'];  
        $offset         = $this->uri->segment(3);  
        $offset         = ( ! is_numeric($offset) || $offset < 1) ? 0 : $offset;  
          
        if(empty($offset))  
        {  
            $offset=0;  
        }  
          
        $this->pagination->initialize($config);         
          
        $data['offset']      = $offset;    
        $data['query']      = $this->db->query($string_query." limit $offset,$num");    
        $data['base']       = $this->config->item('base_url');  
      
        return $data;
	}
	
	function customertambah() {
		$this->load->database();
		$nama=$_POST['nama'];
		$jk=$_POST['jk'];
		$hp=$_POST['hp'];
		$email=$_POST['email'];
		$alamat=$_POST['alamat'];
		$user=$_POST['user'];
		$pass=$_POST['pass'];
		$pass=md5("$pass");
		$querycek = $this->db->query("
			insert into customer values (
				null,
				'$nama',
				'$jk',
				'$hp',
				'$email',
				'$alamat',
				'$user',
				'$pass',
				'Aktif'
			)
		");
		
		$pass=$_POST['pass'];
		$pesan="Terima Kasih Telah mendaftar. Username Anda : $user. Password Anda : $pass";
		$querycek = $this->db->query("
			insert into outbox (
				DestinationNumber,
				TextDecoded
			)
			values (
				'$hp',
				'$pesan'
			)
		");
	}
	
	function customeredit($p) {
		$this->load->database();
		$querycek = $this->db->query("
			select * from 
			customer
			where
				idcustomer='$p'
		");
		return $querycek->result();	
	}
	
	function customereditact($p) {
		$this->load->database();
		$nama=$_POST['nama'];
		$jk=$_POST['jk'];
		$hp=$_POST['hp'];
		$email=$_POST['email'];
		$alamat=$_POST['alamat'];
		$user=$_POST['user'];
		$querycek = $this->db->query("
			update customer set 
				nama='$nama',
				jk='$jk',
				hp='$hp',
				email='$email',
				alamat='$alamat',
				user='$user' 
			where 
				idcustomer='$p'
		");
		if(!empty($_POST['pass'])){
			$pass=$_POST['pass'];
			$pass=md5("$pass");
			$querycek = $this->db->query("
				update customer set 
					pass='$pass' 
				where 
					idcustomer='$p'
			");
		}
	}
	
	function customerhapus($p) {
		$this->load->database();
		$querycek = $this->db->query("delete from customer where idcustomer='$p'");
	}
	
	function kategoriproduk() {
		$this->load->database();
		$this->load->library('pagination');
		if(empty($_POST['cari'])){
			$string_query = "
				select * from 
					kategoriproduk
			";
		}else{
			$cari=$_POST['cari'];
			$string_query = "
				select * from 
					kategoriproduk
				where
					(kategoriproduk like '%$cari%')
			";
		}
        $query = $this->db->query($string_query); 
		$config['base_url']     = base_url().'index.php/home/kategoriproduk/';  
        $config['total_rows']   = $query->num_rows();  
        $config['per_page']     = '5';  
        $num            = $config['per_page'];  
        $offset         = $this->uri->segment(3);  
        $offset         = ( ! is_numeric($offset) || $offset < 1) ? 0 : $offset;  
          
        if(empty($offset))  
        {  
            $offset=0;  
        }  
          
        $this->pagination->initialize($config);         
          
        $data['offset']      = $offset;    
        $data['query']      = $this->db->query($string_query." limit $offset,$num");    
        $data['base']       = $this->config->item('base_url');  
      
        return $data;
	}
	
	function kategoriproduktambah() {
		$this->load->database();
		$kategoriproduk=$_POST['kategoriproduk'];
		$querycek = $this->db->query("
			insert into kategoriproduk values (
				null,
				'$kategoriproduk'
			)
		");
	}
	
	function kategoriprodukedit($p) {
		$this->load->database();
		$querycek = $this->db->query("
			select * from 
				kategoriproduk
			where 
				idkategoriproduk='$p'
		");
		return $querycek->result();	
	}
	
	function kategoriprodukeditact($p) {
		$this->load->database();
		$kategoriproduk=$_POST['kategoriproduk'];
		$querycek = $this->db->query("
			update kategoriproduk set 
				kategoriproduk='$kategoriproduk'
			where 
				idkategoriproduk='$p'
		");
	}
	
	function kategoriprodukhapus($p) {
		$this->load->database();
		$querycek = $this->db->query("delete from kategoriproduk where idkategoriproduk='$p'");
	}
	
	function produk() {
		$this->load->database();
		$this->load->library('pagination');
		if(empty($_POST['cari'])){
			$string_query = "
				select * from 
					produk a,
					kategoriproduk b
				where
					a.idkategoriproduk=b.idkategoriproduk
			";
		}else{
			$cari=$_POST['cari'];
			$string_query = "
				select * from 
					produk a,
					kategoriproduk b
				where
					a.idkategoriproduk=b.idkategoriproduk and
					(a.produk like '%$cari%' or 
					b.kategoriproduk like '%$cari%')
			";
		}
        $query = $this->db->query($string_query); 
		$config['base_url']     = base_url().'index.php/home/produk/';  
        $config['total_rows']   = $query->num_rows();  
        $config['per_page']     = '5';  
        $num            = $config['per_page'];  
        $offset         = $this->uri->segment(3);  
        $offset         = ( ! is_numeric($offset) || $offset < 1) ? 0 : $offset;  
          
        if(empty($offset))  
        {  
            $offset=0;  
        }  
          
        $this->pagination->initialize($config);         
          
        $data['offset']      = $offset;    
        $data['query']      = $this->db->query($string_query." limit $offset,$num");    
        $data['base']       = $this->config->item('base_url');  
      
        return $data;
	}
	
	function produktambah() {
		$this->load->database();
		$idproduk=$_POST['idproduk'];
		$idkategoriproduk=$_POST['idkategoriproduk'];
		$namaproduk=$_POST['namaproduk'];
		$deskripsi=$_POST['deskripsi'];
		$harga=$_POST['harga'];
		$stok=$_POST['stok'];
		
		//upload gambar
		$config['upload_path'] = 'lib/produk';
		$config['allowed_types'] = 'gif|jpg|jpeg|png';
		$this->load->library('upload', $config);
		//nama gambar
		$this->upload->do_upload('gambar');
		$gmbr=$this->upload->data();
		$gambar=$gmbr['file_name'];
		
		$querycek = $this->db->query("
			insert into produk values (
				'$idproduk',
				'$idkategoriproduk',
				'$namaproduk',
				'$deskripsi',
				'$gambar',
				'$harga',
				'$stok'
			)
		");
	}
	
	function produkedit($p) {
		$this->load->database();
		$querycek = $this->db->query("
			select * from 
				produk a,
				kategoriproduk b
			where
				a.idkategoriproduk=b.idkategoriproduk and
				a.idproduk='$p'
		");
		return $querycek->result();	
	}
	
	function produkeditact($p) {
		$this->load->database();
		$idkategoriproduk=$_POST['idkategoriproduk'];
		$namaproduk=$_POST['namaproduk'];
		$deskripsi=$_POST['deskripsi'];
		$harga=$_POST['harga'];
		$stok=$_POST['stok'];
		
		if(!empty($_FILES['gambar']['name'])){
			//upload gambar
			$config['upload_path'] = './lib/produk';
			$config['allowed_types'] = 'gif|jpg|jpeg|png';
			$this->load->library('upload', $config);
			//nama gambar
			$this->upload->do_upload('gambar');
			$gmbr=$this->upload->data();
			$gambar=$gmbr['file_name'];
			
			$querycek = $this->db->query("
				update produk set 
					gambar='$gambar'
				where 
					idproduk='$p'
			");
		}
		
		$querycek = $this->db->query("
			update produk set 
				idkategoriproduk='$idkategoriproduk',
				namaproduk='$namaproduk',
				deskripsi='$deskripsi',
				deskripsi='$deskripsi',
				harga='$harga',
				stok='$stok'
			where 
				idproduk='$p'
		");
	}
	
	function produkhapus($p) {
		$this->load->database();
		$querycek = $this->db->query("delete from produk where idproduk='$p'");
	}
	
}
 
?>