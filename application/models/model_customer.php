<?php
class Model_customer extends CI_Controller {
	function _construct() {
	parent::CI_Controller();
}
	
	function pilihkategoriproduk() {
		$this->load->database();
		$querycek = $this->db->query("
			select * from 
				kategoriproduk
		");
		return $querycek->result();	
	}
	
	function produk($p) {
		$this->load->database();
		$this->load->library('pagination');
		if(empty($_POST['cari'])){
			$string_query = "
				select * from 
					produk a,
					kategoriproduk b
				where
					a.idkategoriproduk=b.idkategoriproduk and
					a.idkategoriproduk='$p'
			";
		}else{
			$cari=$_POST['cari'];
			$string_query = "
				select * from 
					produk a,
					kategoriproduk b
				where
					a.idkategoriproduk=b.idkategoriproduk and
					(a.produk like '%$cari%' or 
					b.kategoriproduk like '%$cari%') and
					a.idkategoriproduk='$p'
			";
		}
        $query = $this->db->query($string_query); 
		$config['base_url']     = base_url().'index.php/customer/produk/'.$p;  
        $config['total_rows']   = $query->num_rows();  
        $config['per_page']     = '20';  
        $num            = $config['per_page'];  
        $offset         = $this->uri->segment(4);  
        $offset         = ( ! is_numeric($offset) || $offset < 1) ? 0 : $offset;  
          
        if(empty($offset))  
        {  
            $offset=0;  
        }  
          
        $this->pagination->initialize($config);         
          
        $data['offset']      = $offset;    
        $data['query']      = $this->db->query($string_query." limit $offset,$num");    
        $data['base']       = $this->config->item('base_url');  
      
        return $data;
	}
	
}
 
?>