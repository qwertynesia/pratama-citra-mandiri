-- phpMyAdmin SQL Dump
-- version 3.2.4
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jul 06, 2014 at 08:37 PM
-- Server version: 5.1.41
-- PHP Version: 5.3.1

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `nicky`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE IF NOT EXISTS `admin` (
  `idadmin` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(40) NOT NULL,
  `tlp` varchar(15) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `level` enum('Admin','User') NOT NULL,
  `status` enum('Aktif','Tidak Aktif') NOT NULL,
  PRIMARY KEY (`idadmin`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`idadmin`, `nama`, `tlp`, `username`, `password`, `level`, `status`) VALUES
(1, 'admin', '085745745745', 'admin', '81dc9bdb52d04dc20036dbd8313ed055', 'Admin', 'Aktif');

-- --------------------------------------------------------

--
-- Table structure for table `customer`
--

CREATE TABLE IF NOT EXISTS `customer` (
  `idcustomer` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(40) NOT NULL,
  `jk` enum('Laki - laki','Perempuan') NOT NULL,
  `hp` varchar(15) NOT NULL,
  `email` varchar(40) NOT NULL,
  `alamat` text NOT NULL,
  `user` varchar(40) NOT NULL,
  `pass` varchar(100) NOT NULL,
  `status` enum('Aktif','Tidak Aktif') NOT NULL,
  PRIMARY KEY (`idcustomer`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `customer`
--

INSERT INTO `customer` (`idcustomer`, `nama`, `jk`, `hp`, `email`, `alamat`, `user`, `pass`, `status`) VALUES
(2, 'danis', 'Laki - laki', '08121502464', 'dani@gmail.com', 'skb', 'dani', '81dc9bdb52d04dc20036dbd8313ed055', 'Aktif');

-- --------------------------------------------------------

--
-- Table structure for table `dataorder`
--

CREATE TABLE IF NOT EXISTS `dataorder` (
  `idorder` varchar(20) NOT NULL,
  `idcustomer` int(11) NOT NULL,
  `idbiayakirim` int(11) NOT NULL,
  `subtotal` int(11) NOT NULL,
  `biayakirim` int(11) NOT NULL,
  `total` int(11) NOT NULL,
  `tgl` date NOT NULL,
  `statusalamat` enum('Alamat 1','Alamat 2') NOT NULL,
  PRIMARY KEY (`idorder`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `dataorder`
--

INSERT INTO `dataorder` (`idorder`, `idcustomer`, `idbiayakirim`, `subtotal`, `biayakirim`, `total`, `tgl`, `statusalamat`) VALUES
('OR001', 2, 1, 350000, 15000, 50000, '2014-06-15', 'Alamat 1');

-- --------------------------------------------------------

--
-- Table structure for table `detailorder`
--

CREATE TABLE IF NOT EXISTS `detailorder` (
  `iddetailorder` int(11) NOT NULL AUTO_INCREMENT,
  `idorder` varchar(20) NOT NULL,
  `idproduk` varchar(20) NOT NULL,
  `idukuranproduk` int(11) NOT NULL,
  `harga` int(11) NOT NULL,
  `jml` int(11) NOT NULL,
  `subharga` int(11) NOT NULL,
  PRIMARY KEY (`iddetailorder`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `detailorder`
--

INSERT INTO `detailorder` (`iddetailorder`, `idorder`, `idproduk`, `idukuranproduk`, `harga`, `jml`, `subharga`) VALUES
(1, 'OR001', 'IF-001', 2, 350000, 1, 350000);

-- --------------------------------------------------------

--
-- Table structure for table `kategoriproduk`
--

CREATE TABLE IF NOT EXISTS `kategoriproduk` (
  `idkategoriproduk` int(11) NOT NULL AUTO_INCREMENT,
  `kategoriproduk` varchar(40) NOT NULL,
  PRIMARY KEY (`idkategoriproduk`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `kategoriproduk`
--

INSERT INTO `kategoriproduk` (`idkategoriproduk`, `kategoriproduk`) VALUES
(1, 'Test Kategori');

-- --------------------------------------------------------

--
-- Table structure for table `konfirmasipembayaran`
--

CREATE TABLE IF NOT EXISTS `konfirmasipembayaran` (
  `idkonfirmasipembayaran` int(11) NOT NULL AUTO_INCREMENT,
  `idorder` varchar(20) NOT NULL,
  `namapengirim` varchar(40) NOT NULL,
  `norek` varchar(40) NOT NULL,
  `bank` varchar(40) NOT NULL,
  `jmltransfer` int(11) NOT NULL,
  `buktitransfer` varchar(40) NOT NULL,
  `tgl` date NOT NULL,
  PRIMARY KEY (`idkonfirmasipembayaran`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `konfirmasipembayaran`
--

INSERT INTO `konfirmasipembayaran` (`idkonfirmasipembayaran`, `idorder`, `namapengirim`, `norek`, `bank`, `jmltransfer`, `buktitransfer`, `tgl`) VALUES
(1, 'OR001', 'Dani', '112221122', 'Mandiri', 50000, '12345', '2014-06-15');

-- --------------------------------------------------------

--
-- Table structure for table `perbaikan`
--

CREATE TABLE IF NOT EXISTS `perbaikan` (
  `idperbaikan` int(11) NOT NULL AUTO_INCREMENT,
  `idcustomer` int(11) NOT NULL,
  `idadmin` int(11) NOT NULL,
  `est_selesai` date NOT NULL,
  `keterangan` text NOT NULL,
  `tgl` date NOT NULL,
  PRIMARY KEY (`idperbaikan`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `perbaikan`
--


-- --------------------------------------------------------

--
-- Table structure for table `produk`
--

CREATE TABLE IF NOT EXISTS `produk` (
  `idproduk` varchar(20) NOT NULL,
  `idkategoriproduk` int(11) NOT NULL,
  `namaproduk` varchar(40) NOT NULL,
  `deskripsi` text NOT NULL,
  `gambar` varchar(100) NOT NULL,
  `harga` int(11) NOT NULL,
  `stok` int(11) NOT NULL,
  PRIMARY KEY (`idproduk`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `produk`
--

INSERT INTO `produk` (`idproduk`, `idkategoriproduk`, `namaproduk`, `deskripsi`, `gambar`, `harga`, `stok`) VALUES
('IF-001', 1, 'Test Produk 2', 'test', 'Koala.jpg', 350000, 10);

-- --------------------------------------------------------

--
-- Table structure for table `progress`
--

CREATE TABLE IF NOT EXISTS `progress` (
  `idprogress` int(11) NOT NULL AUTO_INCREMENT,
  `idperbaikan` int(11) NOT NULL,
  `detail` text NOT NULL,
  `status` enum('belum','selesai') NOT NULL,
  PRIMARY KEY (`idprogress`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `progress`
--


/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
